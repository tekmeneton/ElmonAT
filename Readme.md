
ElmonAT - Script to plot data from APG
======================================

Dieses Skript lädt die Stromerzeugungs-, Stromverbrauchs- und Import-/Exportdaten von der APG-API herunter, wandelt die JSON-Dateien zu CSV-Dateien um und plottet anschließend die Daten mit gnuplot.

ALLE ANGABEN DES APG UND DIESES SKRIPTS OHNE GEWÄHR!

Dieses Projekt ist gänzlich unabhängig von APG und repräsentiert APG in keiner Weise.
Beim Aufrufen des Skripts werden Daten ähnlich wie beim normalen Verwenden der APG-Webseite heruntergeladen.


Online-Plots des APG:
<!-- https://markttransparenz.apg.at/markt/Markttransparenz/erzeugung/Erzeugung%20pro%20Typ  
https://www.apg.at/de/markt/Markttransparenz/last/Ist-Last  
https://markttransparenz.apg.at/de/markt/Markttransparenz/Uebertragung/Lastfluesse  -->
transparency.apg.at, z.B. https://transparency.apg.at/erzeugung-nach-typ/chart?resolution=PT15M


Daten
=====

(siehe auch "Eigenschaften/Eigenheiten" im Unterkapitel "Skript")

Darstellung
-----------
So wie in den Plots auf der APG-Webseite wird das Entleeren der Pumpspeicher zur Stromerzeugung positiv und das Füllen zur Stromspeicherung negativ dargestellt. Mit Import (positiv) und Export (negativ) verhält es sich entsprechend dazu.




Skript
======

Sicherheit
----------
Es wird besonders empfohlen eine aktuelle / gut gewartete Version von `curl` zu verwenden, da es dort in der Vergangenheit gelegentlich möglicherweise relevante Sicherheitslücken gegeben hat. Andere verwendete Programme sollten ebenfalls aktuell / gut gewartet sein, dort sind mir aber noch keine relevanten Sicherheitslücken bekannt. (Da beispielsweise keine Daten aus dem Internet in Umgebungsvariablen gespeichert werden sollte nichts in Richtung des Bugs "Shellshock" funktionieren.)

Dieses Skript lädt mit curl Daten aus dem Internet herunter und speichert sie in Dateien. Anschließend wird das Format in den Dateien mit Programmen geändert die *möglicherweise* nicht für nicht vertrauenswürdige Daten ausgelegt sind\*. Da es dabei nur um Textersetzung geht (größtenteils mit einfachen REGEX) würde ich mir erwarten, dass da nicht viel schiefgehen kann, aber wer weiß - es gibt viele recht verschiedene Implementierungen von diesen Programmen. Die Daten aus dem Internet werden jedenfalls *nicht* als Parameter für die Programme verwendet, die Daten haben also keinen Einfluss darauf welche Textersetzungen vorgenommen werden. Schlussendlich werden die Daten mit gnuplot dargestellt (wobei die Daten ebenfalls nicht als Commands verwendet werden).
Parameter, die dem Skript beim Aufruf übergeben werden, werden als vertrauenswürdig angenommen.
<!-- Hab' mal nachgeschaut und auf die Schnelle keine relevanten CVEs für grep, sed und gnuplot gefunden: Da scheint's vor allem Probleme zu machen falls man die Parameter/Commands für diese Programme kontrollieren kann. Unicodezeichen sollten so wie die Programme verwendet werden auch nicht geparsed werden glaube ich. -->

\* Die Daten von APG sind natürlich sehr wohl vertrauenswürdig, es kann aber - wie überall - nie völlig ausgeschlossen werden dass die Daten nicht von irgendwo anders her kommen. HTTPS sollte das aber natürlich recht sicher machen.

Funktionalität
--------------
Die Daten von APG liegen als JSON-Dateien vor. Weil ich gerne Unsinn mit Textersetzungsprogrammen mache (cat | tr | grep | grep | sed | grep | tr | sed | sed | sed) parse ich die JSON-Datei nicht wirklich sondern wandle sie ein weing umständlich in CSV-Dateien um. Der einzige Vorteil ist vermutlich, dass diese Programme im Gegensatz zu JSON-Parsern häufig in unixoiden Systemen bereits installiert sind. Sollte dieses Skript unerwarteterweise von vielen Personen verwendet werden, werde ich mir überlegen das effizienter zu machen. (Derzeit braucht das Skript bei mir - ohne Download, um den Teil geht es ja nicht - ca. 70ms ohne gnuplot bei 3 genutzten Prozessorkernen, mit gnuplot sind's ca. 200ms auf 2 Kernen - verglichen mit z.B. einem Browser der wasauchimmer macht ist das ausreichend sparsam für mich. *So* oft dass das was ausmacht rufe ich das Skript nicht auf.)
gnuplot kann praktischerweise CSV-Dateien parsen; die Reihenfolge der einzelnen Stromerzeugungsarten kann im Skript recht einfach durch Vertauschen von Zeilen geändert werden.


Kommandozeilenparameter:
------------------------
--date: Datum, für das ein Plot angezeigt werden soll. Dafür wird date -d "..." verwendet. Beispiel: --date="yesterday" , --date="yesterday yesterday", --date="last friday", --date="12 Mar 2020"  
--help: Zeigt den Hilfetext an  
(--debug: Nicht dokumentiert, siehe Inhalt von Skript. Fügt größtenteils ein paar Ausgaben hinzu und lässt gnuplot so laufen, dass man Sachen in dessen Kommandozeile schreiben kann.)  
--redownload: Lädt die Daten für den ausgewählten Tag neu herunter. Für alle Tage bis auf den aktuellen nicht besonders sinnvoll. Nicht zu oft verwenden um keinen unnötigen Traffic bei APG zu verursachen (vgl. auf der Webseite oft im Browser F5 drücken); die Daten für den aktuellen Tag werden ohnehin nur alle 15 Minuten aktualisiert.  
--version: Zeigt Version, Copyright und Lizenz an  

Eigenschaften/Eigenheiten
-------------------------

* Die Import-/Exportdaten gibt es bereits in der Viertelstunde nach dem tatsächlichen Zeitpunkt, bei den anderen Daten dauert es ca. 1h.

* Die Einschränkungen bezüglich der Genauigkeiten auf der APG-Seite gelten natürlich auch hier (z.B. als dieser Text geschrieben wurde basierten Solar-, Wind- und vor allem Kleinanlagendaten teilweise auf Hochrechnungen).

* Eine Zeit lang nach Mitternacht kann es sein, dass die Daten noch nicht vollständig vorliegen; dabei kann es zu Fehlern kommen.
<!-- optionales TODO: hmmm, vielleicht sollte ich mir da irgendwann mal was überlegen] -->

* Wenn es aus irgendeinem Grund beim Herunterladen zu Fehlern gekommen ist (z.B. keine Internetverbindung) muss "--redownload" verwendet werden obwohl noch nichts heruntergeladen wurde.

Temporäre Dateien
-----------------

Die heruntergeladenen Dateien werden in das Verzeichnis /tmp/ElmonAT geschrieben. Dieses Verzeichnis wird vom Skript nie entfernt, bei einigen (vielen?) Linuxdistributionen sollte das Directory aber gelegentlich geleert werden (bei einigen beim Herunterfahren oder Starten). Auf größeren Multi-User-Systemen sollte der Ort möglicherweise geändert werden.

Die CSV-Dateien können natürlich auch mit anderen Programmen geparsed werden, es wird aber nicht garantiert dass sie vernünftigen Inhalt beinhalten oder dass sie sicher/vertrauenswürdig sind (siehe auch Unterpunkt "Sicherheit").

Datenschutz
-----------
Das Skript lädt mit `curl` drei Dateien herunter; es kann davon ausgegangen werden, dass der UserAgent ("curl; ElmonAT/1.0 (codeberg.org/tekmeneton/ElmonAT)"), die IP-Adresse, die abgerufenen Daten und Datum/Uhrzeit der Anfrage bei APG gespeichert werden.


Lizenz / Verändern des Skripts
------------------------------

Copyright 2022 Tekmeneton

"AGPL-3.0-only": Dieses Skript ist definitiv nicht general purpose und inhaltlich *so* einfach, dass APGL v3 auf jeden Fall in Ordnung sein sollte: Falls irgendwem wirklich etwas daran liegt irgendetwas zu machen was mit der Lizenz nicht vereinbar ist, ist es nicht allzu schwierig ein eigenes Skript zu schreiben.

Rechtlich nicht bindende Kurzfassung für Endnutzer: Es gibt keine garantie dafür, dass das Skript richtig funktioniert und dass es keinen groben Unfug macht. Man darf das Skript beliebig privat nutzen und natürlich auch modifizieren und Plots an z.B. Freunde senden oder ähnliches. Wenn sich z.B. Freunde für die Modifikationen interessieren bitte diese weitergeben. Wenn z.B. Plots automatisch generiert und auf einer Webseite gehostet werden und das Programm dafür modifiziert wurde müssen die Änderungen ebenfalls veröffentlicht werden - z.B. einfach beim Plot auf eine auf der Webseite zu findende geänderte Version gut sichtbar verlinken (also kein weißer Text auf weißem Hintergrund oder ähnliches).

<!--
https://www.gnu.org/licenses/gpl-howto.html
https://www.gnu.org/licenses/agpl-3.0.html
-->

Wartung des Skripts
-------------------

Dass das Skript an Änderungen von z.B. transparency.apg.at angepasst wird kann ich nicht garantieren; dadurch könnte unter anderem passieren, dass die Funktionalität beeinträchtigt wird / das Skript nicht mehr funktioniert.



Verschiedenes
=============

Andere eventuell interessante Links:
------------------------------------
Jährliche Statistiken zu Strom und Gas in Österreich: https://www.e-control.at/publikationen/statistik-bericht  
Gasspeicherfüllstände in Europa: https://agsi.gie.eu/#/  
Gaspipelinenutzung von Russland nach Europa: https://berthub.eu/gazmon/  
Plot zur Stromerzeugung in Deutschland: https://www.agora-energiewende.de/service/agorameter/  
Gaspipelinekarte und deren Durchfluss etc. für Europa: https://transparency.entsog.eu  
Details zu Gasspeichernutzung in Österreich: https://energie.gv.at/  
Grafiken zur Elektrizitätsversorgung in Österreich, z.B. auch Pumpspeicherfüllstände (leider nicht tagesaktuell (Stand: 21.1.2023)): https://www.e-control.at/statistik/e-statistik/charts


<!-- optionales TODO: Maybe add some AGPL logo somewhere] -->

